package thread;
import exceptions.Exception4;

import java.lang.Runnable;
public class Boucle implements Runnable {
//C'est l'implémentation de Runnable qui indique qu'on est dans un thread

    String name;
    int interval;
    public Boucle(String name, int interval){
        //Constructeur avec le nom et l'intervalle de pause
        this.name = name;
        this.interval = interval;
    }
    @Override
    public void run() {
        for (int idx = 0; idx < 10; idx++) {
            //Méthode avec des exceptions :
            try {
                //this.testException(idx);
                System.out.printf("%s %d %n", this.name, idx);
                Thread.sleep(this.interval*1000);
                //Thread.sleep(this.interval*1000);
                //Endort le thread
            } /*catch (Exception4 e) {
                System.out.printf("Perdu");
            } */ catch (InterruptedException e) {
            e.printStackTrace();
             }
        }
    }

    //Envoyer une exception avec une méthode
    private void testException (int idxe) throws Exception4 {
        if ((idxe > 3) && (idxe < 5)) {
            throw new Exception4();
        }
    }
}
