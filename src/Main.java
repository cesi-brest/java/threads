import thread.Boucle;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {
        Thread tOne = new Thread(new Boucle("Thread one", 2));
        Thread tTwo = new Thread(new Boucle("Thread two", 3));

        tOne.start();
        tTwo.start();
        }
    }
